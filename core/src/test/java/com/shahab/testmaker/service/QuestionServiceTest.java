package com.shahab.testmaker.service;

import com.shahab.testmaker.model.Question;
import com.shahab.testmaker.model.TestExam;
import com.shahab.testmaker.repository.QuestionRepository;
import com.shahab.testmaker.repository.TestExamRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;
import java.util.UUID;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class QuestionServiceTest {

  private final UUID EXAM_ID = UUID.randomUUID();

  private String QUESTION_TITLE = "title 1";

  @Mock
  private QuestionRepository questionRepository;
  @Mock
  private TestExamRepository testExamRepository;

  @InjectMocks
  QuestionService service;

  @Test
  void addQuestionTest(){
    when(testExamRepository.findById(EXAM_ID)).thenReturn(Optional.of(mock(TestExam.class)));
    Question question = getQuestion(QUESTION_TITLE);
    // When
    service.addQuestionToTestExam(EXAM_ID, question);

    // Then
    verify(testExamRepository).findById(EXAM_ID);
    verify(questionRepository).save(question);
    verify(testExamRepository).save(any());
  }

  @ParameterizedTest
  @NullSource
  void addQuestionWithNullExamIdTest(UUID examId){
    Question question = getQuestion(QUESTION_TITLE);

    // When
    Assertions.assertThrows(IllegalArgumentException.class, ()->
        service.addQuestionToTestExam(examId, question));
  }

  @ParameterizedTest
  @NullSource
  void addQuestionWithNullQuestionTitleTest(String questionTitle){
    Question question = getQuestion(questionTitle);

    // When
    Assertions.assertThrows(IllegalArgumentException.class, ()->
        service.addQuestionToTestExam(EXAM_ID, question));
  }

  private Question getQuestion(String title){
    return new Question(null, 0, title, false, null, null);
  }
}

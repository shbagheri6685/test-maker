package com.shahab.testmaker.service;

import com.shahab.testmaker.model.TestExam;
import com.shahab.testmaker.repository.TestExamRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.UUID;
import java.util.stream.Stream;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class TestExamServiceTest {

  private final UUID EXAM_ID = UUID.randomUUID();

  private static String TESTEXAM_TITLE = "title 1";
  private static String TESTEXAM_DESCRIPTION = "description 1";
  private static String TESTEXAM_PASSWORD = "password1";

  @Mock
  private TestExamRepository testExamRepository;

  @InjectMocks
  TestExamService service;

  @Test
  void createExamTest(){
    TestExam testExam = getTestExam(TESTEXAM_TITLE, TESTEXAM_DESCRIPTION, TESTEXAM_PASSWORD);
    // When
    service.createTestExam(testExam);

    // Then
    verify(testExamRepository).save(testExam);
  }

  @ParameterizedTest
  @MethodSource("invalidTestExamPayload")
  void createExamTestWithInvalidPayload(TestExam testExam){
    Assertions.assertThrows(IllegalArgumentException.class, ()->
        service.createTestExam(testExam));
  }

  private static Stream<Arguments> invalidTestExamPayload() {
    return Stream.of(
        Arguments.of(getTestExam(null, TESTEXAM_DESCRIPTION, TESTEXAM_PASSWORD)),
        Arguments.of(getTestExam(TESTEXAM_TITLE, null, TESTEXAM_PASSWORD)),
        Arguments.of(getTestExam(TESTEXAM_TITLE, TESTEXAM_DESCRIPTION, null))
    );
  }

  private static TestExam getTestExam(String title, String desc, String passwordTomanage){
    return new TestExam(title, desc, null, false, passwordTomanage, 0);
  }
}
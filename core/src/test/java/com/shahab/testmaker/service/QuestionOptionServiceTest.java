package com.shahab.testmaker.service;

import com.shahab.testmaker.model.Question;
import com.shahab.testmaker.model.QuestionOption;
import com.shahab.testmaker.repository.QuestionOptionRepository;
import com.shahab.testmaker.repository.QuestionRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;
import java.util.UUID;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class QuestionOptionServiceTest {

  private final UUID QUESTION_ID = UUID.randomUUID();

  private String QUESTION_OPTION_TEXT = "option 1";

  @Mock
  private QuestionOptionRepository questionOptionRepository;
  @Mock
  private QuestionRepository questionRepository;

  @InjectMocks
  QuestionOptionService service;

  @Test
  void addQuestionTest(){
    when(questionRepository.findById(QUESTION_ID)).thenReturn(Optional.of(mock(Question.class)));
    QuestionOption questionOption = getQuestionOption(QUESTION_OPTION_TEXT);
    // When
    service.addQuestionOptionToQuestion(QUESTION_ID, questionOption,false);

    // Then
    verify(questionRepository).findById(QUESTION_ID);
    verify(questionOptionRepository).save(questionOption);
    verify(questionRepository).save(any());
  }

  @ParameterizedTest
  @NullSource
  void addQuestionWithNullQuestionIdTest(UUID questionId){
    QuestionOption questionOption = getQuestionOption(QUESTION_OPTION_TEXT);
    // When
    Assertions.assertThrows(IllegalArgumentException.class, ()->
        service.addQuestionOptionToQuestion(questionId, questionOption,false));
  }

  @ParameterizedTest
  @NullSource
  void addQuestionWithNullQuestionIdTest(String questionOptionText){
    QuestionOption questionOption = getQuestionOption(questionOptionText);
    // When
    Assertions.assertThrows(IllegalArgumentException.class, ()->
        service.addQuestionOptionToQuestion(QUESTION_ID, questionOption,false));
  }

  private QuestionOption getQuestionOption(String text){
    return new QuestionOption(text, 0);
  }
}
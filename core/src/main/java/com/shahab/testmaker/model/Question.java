package com.shahab.testmaker.model;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

@Document
public class Question {
  @Id
  private UUID id;

  private int order;

  private String title;

  private List<QuestionOption> options;

  private boolean randomOrderedOptions;

  private QuestionOption correctAnswer;

  private String correctAnswerDetails;

  @CreatedDate
  private Instant createdAt;

  public Question() {
  }

  /**
   *
   * @param options the question options
   * @param order option order, 0 by default
   * @param title question title
   * @param randomOrderedOptions if options should be ordered randomly every time
   * @param correctAnswer id of correct option
   * @param correctAnswerDetails correct answer description
   */
  public Question(List<QuestionOption> options, int order, String title, boolean randomOrderedOptions,
                  QuestionOption correctAnswer, String correctAnswerDetails) {
    this.id = UUID.randomUUID();
    this.options = options;
    this.order = order;
    this.title = title;
    this.randomOrderedOptions = randomOrderedOptions;
    this.correctAnswer = correctAnswer;
    this.correctAnswerDetails = correctAnswerDetails;
    this.createdAt = Instant.now();
  }

  public UUID getId() {
    return id;
  }

  public int getOrder() {
    return order;
  }

  public void setOrder(int order) {
    this.order = order;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public List<QuestionOption> getOptions() {
    return options;
  }

  public void setOptions(List<QuestionOption> options) {
    this.options = options;
  }

  public boolean isRandomOrderedOptions() {
    return randomOrderedOptions;
  }

  public void setRandomOrderedOptions(boolean randomOrderedOptions) {
    this.randomOrderedOptions = randomOrderedOptions;
  }

  public QuestionOption getCorrectAnswer() {
    return correctAnswer;
  }

  public void setCorrectAnswer(QuestionOption correctAnswer) {
    this.correctAnswer = correctAnswer;
  }

  public String getCorrectAnswerDetails() {
    return correctAnswerDetails;
  }

  public void setCorrectAnswerDetails(String correctAnswerDetails) {
    this.correctAnswerDetails = correctAnswerDetails;
  }

  public Instant getCreatedAt() {
    return createdAt;
  }
}

package com.shahab.testmaker.model;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Instant;
import java.util.UUID;

@Document
public class QuestionOption {
  @Id
  private UUID id;

  private String text;

  private int order;

  @CreatedDate
  private Instant createdAt;

  public QuestionOption() {
  }

  /**
   *
   * @param text the option text
   * @param order option order, 0 by default
   */
  public QuestionOption(String text, int order) {
    this.id = UUID.randomUUID();
    this.text = text;
    this.order = order;
    this.createdAt = Instant.now();
  }

  public UUID getId() {
    return id;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public int getOrder() {
    return order;
  }

  public void setOrder(int order) {
    this.order = order;
  }

  public Instant getCreatedAt() {
    return createdAt;
  }
}

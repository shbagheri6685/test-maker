package com.shahab.testmaker.model;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

@Document
public class TestExam {
  @Id
  private UUID id;

  private String title;

  private String description;

  private List<Question> questions;

  private boolean randomOrderedQuestions;

  private String passwordToManage;

  private int timePerQuestionInSeconds;

  @CreatedDate
  private Instant createdAt;

  public TestExam() {
  }

  public TestExam(String title, String description) {
    this.title = title;
    this.description = description;
  }

  /**
   * @param title test title
   * @param description test description
   * @param questions test questions
   * @param randomOrderedQuestions if questions should be ordered randomly every time
   * @param passwordToManage the password to manage test
   * @param timePerQuestionInSeconds by default 0 to disable
   */
  public TestExam(String title, String description, List<Question> questions, boolean randomOrderedQuestions, String passwordToManage,
                  int timePerQuestionInSeconds) {
    this.id = UUID.randomUUID();
    this.title = title;
    this.description = description;
    this.questions = questions;
    this.randomOrderedQuestions = randomOrderedQuestions;
    this.passwordToManage = passwordToManage;
    this.timePerQuestionInSeconds = timePerQuestionInSeconds;
    this.createdAt = Instant.now();
  }

  public UUID getId() {
    return id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public List<Question> getQuestions() {
    return questions;
  }

  public void setQuestions(List<Question> questions) {
    this.questions = questions;
  }

  public boolean isRandomOrderedQuestions() {
    return randomOrderedQuestions;
  }

  public void setRandomOrderedQuestions(boolean randomOrderedQuestions) {
    this.randomOrderedQuestions = randomOrderedQuestions;
  }

  public String getPasswordToManage() {
    return passwordToManage;
  }

  public void setPasswordToManage(String passwordToManage) {
    this.passwordToManage = passwordToManage;
  }

  public int getTimePerQuestionInSeconds() {
    return timePerQuestionInSeconds;
  }

  public void setTimePerQuestionInSeconds(int timePerQuestionInSeconds) {
    this.timePerQuestionInSeconds = timePerQuestionInSeconds;
  }

  public Instant getCreatedAt() {
    return createdAt;
  }
}

package com.shahab.testmaker.service;

import com.shahab.testmaker.model.Question;
import com.shahab.testmaker.model.QuestionOption;
import com.shahab.testmaker.repository.QuestionOptionRepository;
import com.shahab.testmaker.repository.QuestionRepository;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Collections;
import java.util.Optional;
import java.util.UUID;

@Service
public class QuestionOptionService {

  private final QuestionOptionRepository questionOptionRepository;
  private final QuestionRepository questionRepository;

  private QuestionOptionService(QuestionRepository questionRepository, QuestionOptionRepository questionOptionRepository) {
    this.questionRepository = questionRepository;
    this.questionOptionRepository = questionOptionRepository;
  }

  /**
   * Add a question option to a question
   * @param questionId question id to add question option
   * @param questionOption question option to be added
   * @param isCorrectChoice true if this option is the correct option of question
   * @return the Question object with updated options
   */
  public Optional<Question> addQuestionOptionToQuestion(UUID questionId, QuestionOption questionOption,
                                                        boolean isCorrectChoice){
    if(questionId ==null) {
      throw new IllegalArgumentException("questionId can not be null");
    }
    validateQuestionOption(questionOption);
    return questionRepository.findById(questionId).map(
        question -> {
          questionOptionRepository.save(questionOption);
          if(question.getOptions()==null){
            question.setOptions(Collections.singletonList(questionOption));
          }else{
            question.getOptions().add(questionOption);
          }
          if(isCorrectChoice){
            question.setCorrectAnswer(questionOption);
          }
          questionRepository.save(question);
          return question;
        }
    );
  }

  private void validateQuestionOption(QuestionOption questionOption)  throws IllegalArgumentException {
    if(StringUtils.isEmpty(questionOption.getText())){
      throw new IllegalArgumentException("text can not be empty");
    }
  }
}
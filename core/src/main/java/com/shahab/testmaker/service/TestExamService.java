package com.shahab.testmaker.service;

import com.shahab.testmaker.model.TestExam;
import com.shahab.testmaker.repository.TestExamRepository;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Optional;
import java.util.UUID;

@Service
public class TestExamService {

  private final TestExamRepository testExamRepository;

  private TestExamService(TestExamRepository testExamRepository) {
    this.testExamRepository = testExamRepository;
  }

  /**
   * Create a test exam
   * @param testExam test exam to be created
   * @return test exam object with Id
   */
  public TestExam createTestExam(TestExam testExam){
    validateTestExam(testExam);
    return testExamRepository.save(testExam);
  }

  public Optional<TestExam> getTestExam(UUID id){
    return testExamRepository.findById(id);
  }

  private void validateTestExam(TestExam testExam)  throws IllegalArgumentException {
    if(StringUtils.isEmpty(testExam.getTitle())){
      throw new IllegalArgumentException("title can not be empty");
    }else if(StringUtils.isEmpty(testExam.getDescription())){
      throw new IllegalArgumentException("description can not be empty");
    }else if(StringUtils.isEmpty(testExam.getPasswordToManage())){
      throw new IllegalArgumentException("password to manage can not be empty");
    }
  }
}
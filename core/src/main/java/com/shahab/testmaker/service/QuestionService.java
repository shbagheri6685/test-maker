package com.shahab.testmaker.service;

import com.shahab.testmaker.model.Question;
import com.shahab.testmaker.repository.QuestionRepository;
import com.shahab.testmaker.repository.TestExamRepository;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Collections;
import java.util.Optional;
import java.util.UUID;

@Service
public class QuestionService {

  private final QuestionRepository questionRepository;
  private final TestExamRepository testExamRepository;

  private QuestionService(TestExamRepository testExamRepository,QuestionRepository questionRepository) {
    this.testExamRepository = testExamRepository;
    this.questionRepository = questionRepository;
  }

  /**
   * Add a question to test exam
   * @param testExamId the test exam id to add question
   * @param question question to be added
   * @return Question object with Id
   */
  public Optional<Question> addQuestionToTestExam(UUID testExamId, Question question){
    if(testExamId ==null) {
      throw new IllegalArgumentException("testExamId can not be null");
    }
    validateQuestion(question);
    return testExamRepository.findById(testExamId).map(
        testExam -> {
          questionRepository.save(question);
          if(testExam.getQuestions()!=null) {
            testExam.getQuestions().add(question);
          }else{
            testExam.setQuestions(Collections.singletonList(question));
          }
          testExamRepository.save(testExam);
          return question;
        }
    );
  }

  private void validateQuestion(Question question)  throws IllegalArgumentException {
    if(question == null){
      throw new IllegalArgumentException("question can not be null");
    }
    if(StringUtils.isEmpty(question.getTitle())){
      throw new IllegalArgumentException("title can not be empty");
    }
  }
}
package com.shahab.testmaker.repository;

import com.shahab.testmaker.model.QuestionOption;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface QuestionOptionRepository extends MongoRepository<QuestionOption, UUID> {

}

openapi: '3.0.0'
info:
  title: TestMaker API
  description: The Test Maker API
  version: '0.1'
  termsOfService: url

paths:
  /api/v1/testmaker/testexam:
    post:
      tags:
        - Test Maker
      summary: Create test exam
      description: Create a test exam
      operationId: createTestExam
      requestBody:
        required: true
        $ref: '#/components/requestBodies/CreateTestExamRequest'
      responses:
        '201':
          $ref: '#/components/responses/IdResponse'
        '400':
          $ref: '#/components/responses/BadRequest'

  /api/v1/testmaker/testexam/{id}:
    parameters:
      - $ref: '#/components/parameters/testExamId'
    get:
      tags:
        - Test Maker
      summary: get test exam by id
      description: get a test exam
      operationId: getTestExam
      responses:
        '200':
          $ref: '#/components/responses/TestExamResponse'
        '400':
          $ref: '#/components/responses/BadRequest'
        '404':
          $ref: '#/components/responses/NotFound'

  /api/v1/testmaker/{id}/question:
    parameters:
      - $ref: '#/components/parameters/testExamId'
    post:
      tags:
        - Test Maker
      summary: Create question
      description: Create question for a test exam
      operationId: createQuestion
      requestBody:
        required: true
        $ref: '#/components/requestBodies/CreateQuestionRequest'
      responses:
        '201':
          $ref: '#/components/responses/IdResponse'
        '400':
          $ref: '#/components/responses/BadRequest'
        '404':
          $ref: '#/components/responses/NotFound'

components:
  parameters:
    testExamId:
      in: path
      name: id
      required: true
      description: The test exam id
      schema:
        type: string
        format: uuid

  requestBodies:
    CreateTestExamRequest:
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/createTestExam'

    CreateQuestionRequest:
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/createQuestion'

  responses:
    # 200
    IdResponse:
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/idResponse'
    # 200
    TestExamResponse:
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/testExamResponse'

    #404
    NotFound:
      description: The resource was not found.
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/Error'

    #404
    BadRequest:
      description: bad request
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/Error'

  schemas:

    createTestExam:
      type: object
      properties:
        title:
          type: string
          description: test title
        description:
          type: string
          description: description of test exam
        randomOrderedQuestions:
          type: boolean
          description: true if test questions order should be random every time
        passwordToManage:
          type: string
          description: the password to manage test exam
        timePerQuestionInSeconds:
          type: int
          description: time per question to answer, 0 for unlimited time

    testExamResponse:
      type: object
      properties:
        id:
          type: string
          format: uuid
          description: test exam id
        title:
          type: string
          description: test title
        description:
          type: string
          description: description of test exam
        randomOrderedQuestions:
          type: boolean
          description: true if test questions order should be random every time
        passwordToManage:
          type: string
          description: the password to manage test exam
        timePerQuestionInSeconds:
          type: int
          description: time per question to answer, 0 for unlimited time
        questions:
          type: array
          items:
            type: questionResponse

    createQuestion:
      type: object
      properties:
        title:
          type: string
          description: question title
        order:
          type: int
          description: question order
        randomOrderedOptions:
          type: boolean
          description: true if question options order should be random every time
        correctAnswer:
          type: createQuestionOption
          description: correct answer
        correctAnswerDetails:
          type: string
          description: correct answer details
        options:
          type: array
          items:
            type: createQuestionOption
          description: correct answer details

    questionResponse:
      type: object
      properties:
        id:
          type: string
          format: uuid
        title:
          type: string
          description: question title
        order:
          type: int
          description: question order
        randomOrderedOptions:
          type: boolean
          description: true if question options order should be random every time
        correctAnswerDetails:
          type: string
          description: correct answer details
        correctAnswer:
          type: questionOptionResponse
          description: correct answer
        options:
          type: array
          items:
            type: questionOptionResponse
          description: correct answer details

    createQuestionOption:
      type: object
      properties:
        text:
          type: string
          description: question option text
        order:
          type: int
          description: question option order

    questionOptionResponse:
      type: object
      properties:
        id:
          type: string
          format: uuid
        text:
          type: string
          description: question option text
        order:
          type: int
          description: question option order


    idResponse:
      type: object
      properties:
        id:
          type: string
          format: uuid
          description: the response id in uuid format

    Error:
      type: object
      properties:
        code:
          type: string
        message:
          type: string
      required:
        - code
        - message
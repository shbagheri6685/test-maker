# test-maker

Test maker is a simple project to create multi choice exam

# Test

Tests developed using junit will be executed automatically when you commit your code in gitlab pipeline

for executing locally you can simple do 

`mvn test`

# Future work


- Fix swagger bug so build step works in pipeline
- Add delete test exam endpoint
- Add delete question
- Add an endpoint to update test exam
- Dockerize it
- deploy docker image in a test environment using pipeline
- Developing and executing component tests in pipeline

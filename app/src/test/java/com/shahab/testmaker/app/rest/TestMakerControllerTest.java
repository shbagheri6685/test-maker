package com.shahab.testmaker.app.rest;

import com.shahab.testmaker.app.mapper.TestMakerMapper;
import com.shahab.testmaker.generated.api.rest.model.*;
import com.shahab.testmaker.model.Question;
import com.shahab.testmaker.model.TestExam;
import com.shahab.testmaker.service.QuestionService;
import com.shahab.testmaker.service.TestExamService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class TestMakerControllerTest {
  @Mock
  private TestExamService testExamService;
  @Mock
  private QuestionService questionService;
  @Mock
  private TestMakerMapper testMakerMapper;
  @Mock
  CreateTestExam createTestExam;
  @Mock
  TestExam testExam;
  @Mock
  TestExamResponse testExamResponse;
  @Mock
  CreateQuestion createQuestion;
  @Mock
  Question question;
  @Mock
  QuestionResponse questionResponse;

  @InjectMocks
  private TestMakerController testMakerController;

  private static final UUID TEST_EXAM_ID = UUID.randomUUID();

  @Test
  @DisplayName("Create a test exam status = Created")
  public void createTestExamTest(){
    when(testMakerMapper.toTestExam(createTestExam)).thenReturn(testExam);
    when(testExamService.createTestExam(testExam)).thenReturn(testExam);

    // When
    ResponseEntity<IdResponse> result = testMakerController.createTestExam(createTestExam);

    // Then
    assertEquals(HttpStatus.CREATED, result.getStatusCode());
  }

  @Test
  @DisplayName("Get a test exam status = OK")
  public void getTestExamTest(){
    when(testExamService.getTestExam(TEST_EXAM_ID)).thenReturn(Optional.of(testExam));
    when(testMakerMapper.toTestExamResponse(testExam)).thenReturn(testExamResponse);

    // When
    ResponseEntity<TestExamResponse> result = testMakerController.getTestExam(TEST_EXAM_ID);

    // Then
    assertEquals(HttpStatus.OK, result.getStatusCode());
  }

  @Test
  @DisplayName("Create a question status = Created")
  public void createQuestionTest(){
    when(testMakerMapper.toQuestion(createQuestion)).thenReturn(question);
    when(questionService.addQuestionToTestExam(TEST_EXAM_ID, question)).thenReturn(Optional.of(question));

    // When
    ResponseEntity<IdResponse> result = testMakerController.createQuestion(TEST_EXAM_ID, createQuestion);

    // Then
    assertEquals(HttpStatus.CREATED, result.getStatusCode());
  }
}

package com.shahab.testmaker.app.mapper;

import com.shahab.testmaker.generated.api.rest.model.*;
import com.shahab.testmaker.model.Question;
import com.shahab.testmaker.model.QuestionOption;
import com.shahab.testmaker.model.TestExam;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class TestMakerMapper {

  /**
   * Convert rest object to internal TestExam model
   * @param createTestExam object to be converted
   * @return converted TestExam object
   */
  public TestExam toTestExam(CreateTestExam createTestExam){
    if(createTestExam == null){
      return null;
    }else{
      return new TestExam(createTestExam.getTitle(),createTestExam.getDescription(),null,
          createTestExam.isRandomOrderedQuestions(), createTestExam.getPasswordToManage(),
          createTestExam.getTimePerQuestionInSeconds());
    }
  }

  public TestExamResponse toTestExamResponse(TestExam testExam){
    if(testExam == null){
      return null;
    }else{
      TestExamResponse response = new TestExamResponse();
      response.setId(testExam.getId());
      response.setTitle(testExam.getTitle());
      response.setDescription(testExam.getDescription());
      response.setRandomOrderedQuestions(testExam.isRandomOrderedQuestions());
      response.setPasswordToManage(testExam.getPasswordToManage());
      response.setTimePerQuestionInSeconds(testExam.getTimePerQuestionInSeconds());

      List questions = Optional.ofNullable(testExam.getQuestions()).orElse(Collections.emptyList()).stream().map(
          question -> toQuestionResponse(question)).collect(Collectors.toList());
      response.setQuestions(questions);

      return response;
    }
  }

  public QuestionResponse toQuestionResponse(Question question){
    if(question == null){
      return null;
    }else{
      QuestionResponse response = new QuestionResponse();
      response.setId(question.getId());
      response.setTitle(question.getTitle());
      response.setOrder(question.getOrder());
      response.setRandomOrderedOptions(question.isRandomOrderedOptions());
      response.setCorrectAnswerDetails(question.getCorrectAnswerDetails());
      response.setCorrectAnswer(toQuestionOptionResponse(question.getCorrectAnswer()));

      List questionOptionResponses = Optional.ofNullable(question.getOptions()).orElse(Collections.emptyList()).stream()
          .map(
              questionOption -> toQuestionOptionResponse(questionOption)).collect(Collectors.toList()
      );
      response.setOptions(questionOptionResponses);

      return response;
    }
  }

  public QuestionOptionResponse toQuestionOptionResponse(QuestionOption questionOption){
    if(questionOption == null){
      return null;
    }else{
      QuestionOptionResponse questionOptionResponse = new QuestionOptionResponse();
      questionOptionResponse.setText(questionOption.getText());
      questionOptionResponse.setOrder(questionOption.getOrder());
      questionOptionResponse.setId(questionOption.getId());
      return questionOptionResponse;
    }
  }

  public Question toQuestion(CreateQuestion createQuestion){
    if(createQuestion == null){
      return null;
    }else{
      List questionOptionResponses = Optional.ofNullable(createQuestion.getOptions()).orElse(Collections.emptyList()).stream()
          .map(
              questionOption -> toQuestionOption(questionOption)).collect(Collectors.toList()
          );
      return new Question(questionOptionResponses, createQuestion.getOrder(), createQuestion.getTitle(),
          createQuestion.isRandomOrderedOptions(), toQuestionOption(createQuestion.getCorrectAnswer()),
          createQuestion.getCorrectAnswerDetails()
          );
    }
  }

  public QuestionOption toQuestionOption(CreateQuestionOption createQuestionOption){
    if(createQuestionOption == null){
      return null;
    }else{
      return new QuestionOption(createQuestionOption.getText(), createQuestionOption.getOrder());
    }
  }
}
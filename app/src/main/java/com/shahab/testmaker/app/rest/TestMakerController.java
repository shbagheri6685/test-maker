package com.shahab.testmaker.app.rest;

import com.shahab.testmaker.app.mapper.TestMakerMapper;
import com.shahab.testmaker.generated.api.rest.TestMakerApi;
import com.shahab.testmaker.generated.api.rest.model.CreateQuestion;
import com.shahab.testmaker.generated.api.rest.model.CreateTestExam;
import com.shahab.testmaker.generated.api.rest.model.IdResponse;
import com.shahab.testmaker.generated.api.rest.model.TestExamResponse;
import com.shahab.testmaker.model.Question;
import com.shahab.testmaker.model.TestExam;
import com.shahab.testmaker.service.QuestionService;
import com.shahab.testmaker.service.TestExamService;
import io.swagger.annotations.Api;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;
import java.util.UUID;

@RestController
@Api(tags = "Test Maker")
public class TestMakerController implements TestMakerApi {

  private TestExamService testExamService;
  private QuestionService questionService;
  private TestMakerMapper testMakerMapper;

  public TestMakerController(TestExamService testExamService, QuestionService questionService,
                             TestMakerMapper testMakerMapper) {
    this.testExamService = testExamService;
    this.questionService = questionService;
    this.testMakerMapper = testMakerMapper;
  }

  @Override
  public ResponseEntity<IdResponse> createTestExam(CreateTestExam body) {
    try {
      TestExam testExam = testMakerMapper.toTestExam(body);
      testExam = testExamService.createTestExam(testExam);
      return new ResponseEntity(new IdResponse().id(testExam.getId()), HttpStatus.CREATED);
    } catch (IllegalArgumentException e) {
      return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  @Override
  public ResponseEntity<TestExamResponse> getTestExam(UUID id) {
    try {
      Optional<TestExam> testExam = testExamService.getTestExam(id);
      if (testExam.isPresent()) {
        return ResponseEntity.ok(testMakerMapper.toTestExamResponse(testExam.get()));
      } else {
        return ResponseEntity.notFound().build();
      }
    } catch (IllegalArgumentException e) {
      return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  @Override
  public ResponseEntity<IdResponse> createQuestion(UUID testExamId, CreateQuestion body) {
    try {
      Question question = testMakerMapper.toQuestion(body);
      Optional<Question> savedQuestion = questionService.addQuestionToTestExam(testExamId, question);
      if (savedQuestion.isPresent()) {
        return new ResponseEntity(new IdResponse().id(savedQuestion.get().getId()), HttpStatus.CREATED);
      } else {
        return ResponseEntity.notFound().build();
      }
    } catch (IllegalArgumentException e) {
      return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
    }

  }
}
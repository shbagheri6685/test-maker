package com.shahab.testmaker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = { "com.shahab.testmaker" })
public class TestMakerApplication {

  public static void main(String[] args) {
    SpringApplication.run(TestMakerApplication.class, args);
  }
}